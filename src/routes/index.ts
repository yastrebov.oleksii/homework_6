import express from 'express';
import {
   getAllStudents,
   getStudentById,
   addStudent,
   updateStudent,
   deleteStudent,
   addGroupToStudent,
} from '../controllers/studentController';
import {
   getAllGroups,
   getGroupById,
   addGroup,
   updateGroup,
   deleteGroup,
} from '../controllers/groupController';

const router = express.Router();

router.get('/students', getAllStudents);
router.get('/students/:id', getStudentById);
router.post('/students', addStudent);
router.put('/students/:id', updateStudent);
router.delete('/students/:id', deleteStudent);
router.put('/students/:studentId/group/:groupId', addGroupToStudent);

router.get('/groups', getAllGroups);
router.get('/groups/:id', getGroupById);
router.post('/groups', addGroup);
router.put('/groups/:id', updateGroup);
router.delete('/groups/:id', deleteGroup);

export default router;
