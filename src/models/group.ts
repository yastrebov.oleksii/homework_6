import ObjectId from 'bson-objectid';

export interface Group {
   id: ObjectId;
   name: string;
   students: ObjectId[];
}
