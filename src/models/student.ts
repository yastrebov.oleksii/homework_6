import ObjectId from 'bson-objectid';

export interface Student {
   id: ObjectId;
   name: string;
   surname: string;
   email: string;
   age: number;
   imagePath: string;
   groupId: ObjectId;
}
