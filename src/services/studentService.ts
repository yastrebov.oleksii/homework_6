import ObjectId from 'bson-objectid';
import { Student } from '../models/student';

export class StudentService {
   private students: Student[] = [];

   getAllStudents(): Student[] {
      return this.students;
   }

   getStudentById(id: ObjectId): Student | null {
      return this.students.find((student) => student.id.equals(id)) || null;
   }

   addStudent(student: Student): void {
      this.students.push(student);
   }

   updateStudent(student: Student): void {
      const index = this.students.findIndex((s) => s.id.equals(student.id));
      if (index !== -1) {
         this.students[index] = student;
      }
   }

   deleteStudent(id: ObjectId): void {
      this.students = this.students.filter((student) => !student.id.equals(id));
   }

   addGroupToStudent(studentId: ObjectId, groupId: ObjectId): void {
      const student = this.students.find((s) => s.id.equals(studentId));
      if (student) {
         student.groupId = groupId;
      }
   }
}
