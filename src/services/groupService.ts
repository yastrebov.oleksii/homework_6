import ObjectId from 'bson-objectid';
import { Group } from '../models/group';

export class GroupService {
   private groups: Group[] = [];

   getAllGroups(): Group[] {
      return this.groups;
   }

   getGroupById(id: ObjectId): Group | null {
      return this.groups.find((group) => group.id.equals(id)) || null;
   }

   addGroup(group: Group): void {
      this.groups.push(group);
   }

   updateGroup(group: Group): void {
      const index = this.groups.findIndex((g) => g.id.equals(group.id));
      if (index !== -1) {
         this.groups[index] = group;
      }
   }

   deleteGroup(id: ObjectId): void {
      this.groups = this.groups.filter((group) => !group.id.equals(id));
   }
}
