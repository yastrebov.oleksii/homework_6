import express from 'express';
import 'dotenv/config';
import multer from 'multer';
import routes from './routes';

const port = process.env.SERVER_PORT || 3000
const app = express();
const upload = multer({ dest: 'uploads/' });

app.use(express.json());
app.use('/api', routes);

app.post('/api/students/:id/avatar', upload.single('avatar'), (req, res) => {
   // Handle image upload logic here
   const { id } = req.params;
   const file = req.file;
   console.log(`Uploaded avatar for student with ID ${id}:`, file);

   res.sendStatus(204);
});

app.get('/api/students/:id/avatar', (req, res) => {
   // Handle image retrieval logic here
   const { id } = req.params;
   console.log(`Retrieving avatar for student with ID ${id}`);

   res.sendStatus(200);
});

app.listen(port, () => {
   console.log(`Server is running on port ${port}`);
});
