import { Request, Response } from 'express';
import ObjectId from 'bson-objectid';
import { GroupService } from '../services/groupService';

const groupService = new GroupService();

export const getAllGroups = (req: Request, res: Response): void => {
   const groups = groupService.getAllGroups();
   res.json(groups);
};

export const getGroupById = (req: Request, res: Response): void => {
   const { id } = req.params;
   const groupId = new ObjectId(id);

   const group = groupService.getGroupById(groupId);
   if (group) {
      res.json(group);
   } else {
      res.status(404).json({ error: 'Group not found' });
   }
};

export const addGroup = (req: Request, res: Response): void => {
   const group = req.body;
   groupService.addGroup(group);
   res.status(201).json(group);
};

export const updateGroup = (req: Request, res: Response): void => {
   const { id } = req.params;
   const groupId = new ObjectId(id);

   const updatedGroup = req.body;
   updatedGroup.id = groupId;

   groupService.updateGroup(updatedGroup);
   res.json(updatedGroup);
};

export const deleteGroup = (req: Request, res: Response): void => {
   const { id } = req.params;
   const groupId = new ObjectId(id);

   groupService.deleteGroup(groupId);
   res.sendStatus(204);
};
