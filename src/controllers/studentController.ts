import { Request, Response } from 'express';
import ObjectId from 'bson-objectid';
import { StudentService } from '../services/studentService';

const studentService = new StudentService();

export const getAllStudents = (req: Request, res: Response): void => {
   const students = studentService.getAllStudents();
   res.json(students);
};

export const getStudentById = (req: Request, res: Response): void => {
   const { id } = req.params;
   const studentId = new ObjectId(id);

   const student = studentService.getStudentById(studentId);
   if (student) {
      res.json(student);
   } else {
      res.status(404).json({ error: 'Student not found' });
   }
};

export const addStudent = (req: Request, res: Response): void => {
   const student = req.body;
   studentService.addStudent(student);
   res.status(201).json(student);
};

export const updateStudent = (req: Request, res: Response): void => {
   const { id } = req.params;
   const studentId = new ObjectId(id);

   const updatedStudent = req.body;
   updatedStudent.id = studentId;

   studentService.updateStudent(updatedStudent);
   res.json(updatedStudent);
};

export const deleteStudent = (req: Request, res: Response): void => {
   const { id } = req.params;
   const studentId = new ObjectId(id);

   studentService.deleteStudent(studentId);
   res.sendStatus(204);
};

export const addGroupToStudent = (req: Request, res: Response): void => {
   const { studentId, groupId } = req.params;
   const studentObjectId = new ObjectId(studentId);
   const groupObjectId = new ObjectId(groupId);

   studentService.addGroupToStudent(studentObjectId, groupObjectId);
   res.sendStatus(204);
};
